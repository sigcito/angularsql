CREATE DATABASE contactos_db;

use contactos_db;

CREATE TABLE contacto(
    id INT(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    nombre VARCHAR(180),
    nombre_empresa VARCHAR(120),
    email VARCHAR (130),
    telefono VARCHAR (35),
    categoria_id INT (2),
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE categoria(
    id INT(2) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    categoria varchar(20)
);