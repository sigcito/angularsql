import { Router } from 'express';
import contactoController from '../controllers/contactoController'

class CategoriaRoutes{

    public router: Router = Router();

    constructor(){
        this.config();
    }

    config(): void{
        this.router.get('/add', contactoController.listCategoria);
    }

}

const categoriaRoutes = new CategoriaRoutes();
export default categoriaRoutes.router;