import {Request, Response} from 'express';
import pool from '../database';

class CategoriaController{

    public async listCategoria (req: Request, res: Response) {
        await pool.query('SELECT * FROM categoria', function(err, result, fields) {
            if (err) throw err;
            res.json(result);
        });
    }

}

export const categoriaController = new CategoriaController();
export default categoriaController;