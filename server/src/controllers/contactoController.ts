import {Request, Response} from 'express';
import pool from '../database';

class ContactoController{

    public async list (req: Request, res: Response) {
        await pool.query('SELECT * FROM contacto', function(err, result, fields) {
            if (err) throw err;
            res.json(result);
        });
    }

    public async listCategoria (req: Request, res: Response) {
        await pool.query('SELECT * FROM categoria', function(err, result, fields) {
            if (err) throw err;
            res.json(result);
        });
    }

    public async getOne (req: Request, res: Response): Promise<any> {
        const { id } = req.params; 
        await pool.query('SELECT * FROM contacto WHERE id = ?', [id], function(err, result, fields) {
            if (err) throw err;
            res.json(result);
        });
    }

    public async create(req: Request, res: Response): Promise<void>{
        //console.log(req.body);
        await pool.query('INSERT INTO contacto SET ?', [req.body])
        res.json({text: 'Contacto Guardado'});
    }
    
    public async delete(req: Request, res: Response): Promise<void>{
        //res.json({text: 'Contacto eliminado' + req.params.id});
        const { id } = req.params;
        await pool.query('DELETE FROM contacto WHERE id = ?', [id]);
        res.json({text: 'El contacto fue Eliminado'});
    }

    public async update(req: Request, res: Response): Promise<void> {
        const { id } = req.params;
        const oldContacto = req.body;
        await pool.query('UPDATE contacto set ? WHERE id = ?', [req.body, id]);
        res.json({ message: "El contacto fue actualizado" });
    }

}

export const contactoController = new ContactoController();
export default contactoController;